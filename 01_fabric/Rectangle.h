#pragma once
#include "IPrimitive.h"

class Rectangle : public APrimitive
{
public:
	Rectangle() = default;
	~Rectangle() override = default;

	void Draw() override;
};

