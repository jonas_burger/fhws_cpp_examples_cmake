#pragma once
#include "../01_fabric/Ellipse.h"
#include "IDrawingItem.h"

class EllipseDrawing :
	public Ellipse, public IDrawingItem
{
public:
	EllipseDrawing() = default;
	EllipseDrawing(int X, int Y, int width, int height);
	~EllipseDrawing() override = default;

	void DrawItem() override;
	void AddItem(DrawingItemPtr item) override;
};

