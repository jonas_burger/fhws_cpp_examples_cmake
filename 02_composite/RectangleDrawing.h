#pragma once
#include "../01_fabric/Rectangle.h"
#include "IDrawingItem.h"

class RectangleDrawing :
	public Rectangle, public IDrawingItem
{
public:
	RectangleDrawing() = default;
	RectangleDrawing(int X, int Y, int width, int height);
	~RectangleDrawing() override = default;
	
	void DrawItem() override;
	void AddItem(DrawingItemPtr item) override;
};

